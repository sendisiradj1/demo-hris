package com.example.demo.controller;

import com.example.demo.model.KaryawanModel;
import com.example.demo.service.KaryawanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class KaryawanController {
    @Autowired
    KaryawanService karyawanService;

    @GetMapping("/karyawan/viewall")
    public String listKaryawan(Model model) {
        List<KaryawanModel> listKaryawan = karyawanService.getListKaryawan();
        model.addAttribute("listKaryawan", listKaryawan);
        return "viewall-karyawan";
    }
}
