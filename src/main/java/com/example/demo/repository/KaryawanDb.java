package com.example.demo.repository;

import com.example.demo.model.KaryawanModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface KaryawanDb extends JpaRepository<KaryawanModel, Long> {
    Optional<KaryawanModel> findById(Long Id);
}
