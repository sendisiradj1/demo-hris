package com.example.demo.service;

import com.example.demo.model.KaryawanModel;
import com.example.demo.repository.KaryawanDb;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@Transactional
public class KaryawanService {
    @Autowired
    KaryawanDb karyawanDb;

    public List<KaryawanModel> getListKaryawan() {
        return karyawanDb.findAll();
    }

    public void addKaryawan(KaryawanModel karyawan) {
        karyawanDb.save(karyawan);
    }
}
